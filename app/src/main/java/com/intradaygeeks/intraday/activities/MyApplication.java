package com.intradaygeeks.intraday.activities;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;
import androidx.multidex.MultiDex;

import com.onesignal.OneSignal;

public class MyApplication extends Application {

    private static MyApplication mInstance;
    public SharedPreferences preferences;
    Activity activity;
    public String prefName = "news";

    public MyApplication() {
        mInstance = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        OneSignal.startInit(this)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    public void doAfterLogin(AppCompatActivity activity, Boolean finishCurrentActivity) {
        long nid = getStartupTimeNID();
        String url = getStartupTimeURL();
        if(nid == 0) {
            if (url.equals("") || url.equals("no_url")) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                activity.startActivity(intent);
                if(finishCurrentActivity) {
                    activity.finish();
                }
            } else {
                Intent a = new Intent(getApplicationContext(), MainActivity.class);
                activity.startActivity(a);

                Intent b = new Intent(getApplicationContext(), ActivityWebView.class);
                b.putExtra("url", url);
                activity.startActivity(b);
                if(finishCurrentActivity) {
                    activity.finish();
                }
            }
        } else {
            Intent intent = new Intent(getApplicationContext(), ActivityOneSignalDetail.class);
            intent.putExtra("id", nid);
            activity.startActivity(intent);
            if(finishCurrentActivity) {
                activity.finish();
            }
        }
    }

    public void saveStartupTimeNID(long nid) {
        preferences = this.getSharedPreferences(prefName, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong("StartupTimeNID", nid);
        editor.commit();
    }

    public long getStartupTimeNID() {
        preferences = this.getSharedPreferences(prefName, 0);
        if (preferences != null) {
            long nid = preferences.getLong(
                    "StartupTimeNID", 0);
            return nid;
        }
        return 0;
    }

    public void saveStartupTimeURL(String url) {
        preferences = this.getSharedPreferences(prefName, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("StartupTimeURL", url);
        editor.commit();
    }

    public String getStartupTimeURL() {
        preferences = this.getSharedPreferences(prefName, 0);
        if (preferences != null) {
            String url = preferences.getString(
                    "StartupTimeURL", "");
            return url;
        }
        return "";
    }

    public void saveIsLogin(boolean flag) {
        preferences = this.getSharedPreferences(prefName, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("IsLoggedIn", flag);
        editor.commit();
    }

    public boolean getIsLogin() {
        preferences = this.getSharedPreferences(prefName, 0);
        if (preferences != null) {
            boolean flag = preferences.getBoolean(
                    "IsLoggedIn", false);
            return flag;
        }
        return false;
    }

    public void saveLogin(String user_id, String user_name, String email) {
        preferences = this.getSharedPreferences(prefName, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("user_id", user_id);
        editor.putString("user_name", user_name);
        editor.putString("email", email);
        editor.commit();
    }

    public String getUserId() {
        preferences = this.getSharedPreferences(prefName, 0);
        if (preferences != null) {
            String user_id = preferences.getString(
                    "user_id", "");
            return user_id;
        }
        return "";
    }

    public String getUserName() {
        preferences = this.getSharedPreferences(prefName, 0);
        if (preferences != null) {
            String user_name = preferences.getString(
                    "user_name", "");
            return user_name;
        }
        return "";
    }

    public String getUserEmail() {
        preferences = this.getSharedPreferences(prefName, 0);
        if (preferences != null) {
            String user_email = preferences.getString(
                    "email", "");
            return user_email;
        }
        return "";
    }

    public String getType() {
        preferences = this.getSharedPreferences(prefName, 0);
        if (preferences != null) {
            String user_type = preferences.getString(
                    "type", "");
            return user_type;
        }
        return "";
    }

    public void saveType(String type) {
        preferences = this.getSharedPreferences(prefName, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("type", type);
        editor.commit();
    }

}
