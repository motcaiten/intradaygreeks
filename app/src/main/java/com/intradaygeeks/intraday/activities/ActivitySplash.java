package com.intradaygeeks.intraday.activities;

import android.content.Intent;
import android.util.Log;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.intradaygeeks.intraday.Config;
import com.intradaygeeks.intraday.R;
import java.util.Iterator;
import java.util.Set;

public class ActivitySplash extends AppCompatActivity {

    Boolean isCancelled = false;
    private ProgressBar progressBar;
    long nid = 0;
    String url = "";
    MyApplication MyApp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        MyApp = MyApplication.getInstance();
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        dumpIntent(getIntent());
        if(getIntent().hasExtra("nid")) {
            nid = getIntent().getLongExtra("nid", 0);
            url = getIntent().getStringExtra("external_link");
        }

        MyApp.saveStartupTimeNID(nid);
        MyApp.saveStartupTimeURL(url);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(!isCancelled) {

                    if(MyApp.getIsLogin()) {
                        MyApp.doAfterLogin(ActivitySplash.this, true);
                    } else {
                        Intent intent = new Intent(getApplicationContext(), ActivityUserLogin.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        },Config.SPLASH_TIME);

    }

    public static void dumpIntent(Intent i){

        Bundle bundle = i.getExtras();
        if (bundle != null) {
            Set<String> keys = bundle.keySet();
            Iterator<String> it = keys.iterator();
            Log.e("DumpIntent","Dumping Intent start");
            while (it.hasNext()) {
                String key = it.next();
                Log.e("DumpIntent","[" + key + "=" + bundle.get(key)+"]");
            }
            Log.e("DumpIntent","Dumping Intent end");
        }
    }
}
