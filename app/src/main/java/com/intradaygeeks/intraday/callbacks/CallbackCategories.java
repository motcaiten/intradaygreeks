package com.intradaygeeks.intraday.callbacks;


import com.intradaygeeks.intraday.models.Category;

import java.util.ArrayList;
import java.util.List;

public class CallbackCategories {
    public String status = "";
    public int count = -1;
    public List<Category> categories = new ArrayList<>();
}
